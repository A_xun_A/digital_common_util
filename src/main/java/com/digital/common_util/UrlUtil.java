
package com.digital.common_util;

/**
 * Created by fangzhengyou on 15/9/5.
 * Log显示工具类
 */
public class UrlUtil {

    public static String addParamToUrl(String curUrl,String param){
        if(curUrl==null)return "";
        String resultUrl=curUrl.split("#")[0];
        String hash=null; //#12222 表示描点
        if(curUrl.contains("#")){
            hash=curUrl.split("#")[1];
        }

        if(curUrl.contains("?")){
            resultUrl+="&"+param;
        }else if(!StringUtil.isNull(param)){
            resultUrl+="?"+param;
        }
        resultUrl+=StringUtil.isNull(hash)?"":("#"+hash);
        return resultUrl;
    }


}
